/**
 * Copyright (c) 2020 Raspberry Pi (Trading) Ltd.
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */

#include <stdio.h>
#include "pico/stdlib.h"
#include "pico/cyw43_arch.h"

#include "hardware/structs/rosc.h"
#include "hardware/gpio.h"

#include <string.h>
#include <time.h>


#include "lwip/pbuf.h"
#include "lwip/tcp.h"
#include "lwip/dns.h"

#include "lwip/altcp_tcp.h"
#include "lwip/altcp_tls.h"
#include "lwip/apps/mqtt.h"

#include "lwip/apps/mqtt_priv.h"





#define DEBUG_printf printf

#define MQTT_SERVER_HOST "telemetria.inti.gob.ar"
#define MQTT_SERVER_PORT 1883

#define PUBLISH_TOPIC  "inti/dma/pruebas/humedad"

#define WILL_TOPIC "inti/dma/pruebas/temperature"        /*uvyt: ultima voluntad y testamentp*/
#define WILL_MSG "sin conexion a broker"             /*uvyt: ultima voluntad y testamentp*/

#define MS_PUBLISH_PERIOD 5000
#define MQTT_TLS 0 // needs to be 1 for AWS IoT

typedef struct MQTT_CLIENT_T_ {
    ip_addr_t remote_addr;
    mqtt_client_t *mqtt_client;
    u8_t receiving;
    u32_t received;
    u32_t counter;
    u32_t reconnect;
} MQTT_CLIENT_T;

const uint LED_PIN = 16;
const uint DOOR_SWITCH_PIN = 17;
const uint MAX_TIMINGS = 85;

err_t mqtt_app_connect(MQTT_CLIENT_T *client);
void perform_payload( char *p);

// Perform initialisation
static MQTT_CLIENT_T* mqtt_client_init(void)
{ 
    MQTT_CLIENT_T *client = calloc(1, sizeof(MQTT_CLIENT_T));
    
    if (!client) {
        DEBUG_printf("failed to allocate state\n");
        return NULL;
    }
    client->receiving = 0;
    client->received = 0;
    return client;
}

void dns_found(const char *name, const ip_addr_t *ipaddr, void *callback_arg)
{
    MQTT_CLIENT_T *client = (MQTT_CLIENT_T*)callback_arg;
    DEBUG_printf("DNS query finished with resolved addr of %s.\n", ip4addr_ntoa(ipaddr));
    client->remote_addr = *ipaddr;
}

void run_dns_lookup(MQTT_CLIENT_T *client)
{
    DEBUG_printf("Running DNS query for %s.\n", MQTT_SERVER_HOST);

    cyw43_arch_lwip_begin();
    err_t err = dns_gethostbyname(MQTT_SERVER_HOST, &(client->remote_addr), dns_found, client);
    cyw43_arch_lwip_end();

    if (err == ERR_ARG) {
        DEBUG_printf("failed to start DNS query\n");
        return;
    }

    if (err == ERR_OK) {
        DEBUG_printf("no lookup needed");
        return;
    }

    while (client->remote_addr.addr == 0) {
        cyw43_arch_poll();
        sleep_ms(1);
    }
}

static void mqtt_connection_cb(mqtt_client_t *client, void *arg, mqtt_connection_status_t status)
{
    //MQTT_CLIENT_T *client = (MQTT_CLIENT_T *)arg;
    if (status != 0) {
        DEBUG_printf("Error during connection: err %d.\n", status);
    } else {
        DEBUG_printf("MQTT connected.\n");
    }
}

/* 
 * Called when publish is complete either with success or failure
 */
void mqtt_pub_request_cb(void *arg, err_t err) {
    MQTT_CLIENT_T *client = (MQTT_CLIENT_T *)arg;
    DEBUG_printf("msg num: %d \n",client->receiving);
    client->receiving++;
    client->received++;
}

/*
 * The app publishing
 */
err_t mqtt_app_publish(MQTT_CLIENT_T *client, char *payload)
{
    //char payload[128];
    err_t err;
    u8_t qos = 2;       /* 0 1 or 2, see MQTT specification */
    u8_t retain = 0;

    int hours, min, sec, day, month, year;
    time_t now;

    time(&now);

    // Convertir al formato de hora local e imprimir a stdout
   // printf("Today is %s", ctime(&now));
 
    // localtime convierte un valor de `time_t` a la hora del calendario y
    // devuelve un puntero a una estructura `tm` con sus miembros
    // rellenado con los valores correspondientes
    struct tm *local = localtime(&now);
 
    hours = local->tm_hour;         // obtener horas desde la medianoche (0-23)
    min = local->tm_min;        // obtener minutos pasados después de la hora (0-59)
    sec = local->tm_sec;        // obtener segundos pasados después de un minuto (0-59)
 
    day = local->tm_mday;            // obtener el día del mes (1 a 31)
    month = local->tm_mon+1;      // obtener el mes del año (0 a 11)
    year = local->tm_year+1970;   // obtener el año desde 1900
 
    // imprime la hora local
    if (hours < 12) {    // antes del mediodia
        printf("Time is %02d:%02d:%02d am\n", hours, min, sec);
    }
    else {    // Después de mediodía
        printf("Time is %02d:%02d:%02d pm\n", hours - 12, min, sec);
    }
 
    // imprime la fecha actual
    printf("Date is: %02d/%02d/%d\n", day, month, year);





    // prepare payload to publish
    sprintf(payload, "hello from picow %d/%d", client->received, client->counter);
    cyw43_arch_gpio_put(CYW43_WL_GPIO_LED_PIN, 1);      //prendo led al enviar dato
    cyw43_arch_lwip_begin();
    err = mqtt_publish(client->mqtt_client, PUBLISH_TOPIC , payload, strlen(payload), qos, retain, mqtt_pub_request_cb, client);
    cyw43_arch_lwip_end();
    err = mqtt_publish(client->mqtt_client, PUBLISH_TOPIC , local, strlen(local), qos, retain, mqtt_pub_request_cb, client);
    //cyw43_arch_gpio_put(CYW43_WL_GPIO_LED_PIN, 0);       //apago led
    if(err != ERR_OK) {
        DEBUG_printf("**** Publish fail***** %d/%d\n", err, ERR_OK);
    }

    return err;
}

/* Initiate client and connect to server, if this fails immediately an error code is returned
 * otherwise mqtt_connection_cb will be called with connection result after attempting to
 * to establish a connection with the server. For now MQTT version 3.1.1 is always used
 */
err_t mqtt_app_connect(MQTT_CLIENT_T *client)
{
    struct mqtt_connect_client_info_t ci;
    err_t err;

    memset(&ci, 0, sizeof(ci));

    ci.client_id = "PicoW";
    ci.client_user = NULL;
    ci.client_pass = NULL;
    // ci.keep_alive = 0;
    // ci.will_topic = NULL;
    // ci.will_msg = NULL;
    // ci.will_retain = 0;
    // ci.will_qos = 0;
    ci.keep_alive = 60;
    ci.will_topic = WILL_TOPIC;
    ci.will_msg = WILL_MSG;
    ci.will_retain = 0;
    ci.will_qos = 2;

    err = mqtt_client_connect(client->mqtt_client, &(client->remote_addr), MQTT_SERVER_PORT, mqtt_connection_cb, client, &ci);
    //original err = mqtt_client_connect(state->mqtt_client, &(state->remote_addr), MQTT_SERVER_PORT, mqtt_connection_cb, state, &ci, mqtt_test_conn_config_cb);
    //err_t          mqtt_client_connect(mqtt_ent_t  client, nst ip_addr_t *ipaddr, u16_t port,       mqtt_connection_cb, *arg ,  ci);

    if (err != ERR_OK) {
        DEBUG_printf("mqtt_connect FAIL \n");
        cyw43_arch_gpio_put(CYW43_WL_GPIO_LED_PIN, 0);
    }

    return err;
}

void app_run_in_loop(MQTT_CLIENT_T *client)
{
    client->mqtt_client = mqtt_client_new();
    client->counter = 0;

    err_t error;
    u32_t publish_period = MS_PUBLISH_PERIOD; // in ms

    char payload[128];

    if (client->mqtt_client == NULL) {
        DEBUG_printf("Failed to create new mqtt client\n");
        return;
    }

    if (mqtt_app_connect(client) == ERR_OK) {

        while (true) {
            cyw43_arch_poll();
            sleep_ms(1);       //1seg
            //sleep_ms(2000);
            cyw43_arch_gpio_put(CYW43_WL_GPIO_LED_PIN, 0);
            //sleep_ms(2000);
            
            if ( !publish_period-- ) {
                if (mqtt_client_is_connected(client->mqtt_client)) {
             
                    //cyw43_arch_lwip_begin();
                    client->receiving = 1;

                    perform_payload( payload );
                    error = mqtt_app_publish(client, payload);   // <-- Publish!

                    if ( error == ERR_OK) {
                        DEBUG_printf("publish numero: %d\n", client->counter);
                        client->counter++;
                    } // else ringbuffer is full and we need to wait for messages to flush.
                    
                    //cyw43_arch_lwip_end();
                }
                else {
                    DEBUG_printf(".");
                }

                publish_period = MS_PUBLISH_PERIOD;
            }
        }
    }
    else
        DEBUG_printf("Failed mqtt_test_connect()\n");

}

void perform_payload( char *p)
{
    time_t s;
    struct tm* current_time;

    char status[10];
       
    if( gpio_get(DOOR_SWITCH_PIN) ){
        gpio_put(LED_PIN, 1);
        strcpy(status,"ON");
    }
    else{
        gpio_put(LED_PIN, 0);
        strcpy(status,"OFF");
    }

    // time in seconds
    s = time(NULL);
     // to get current time
    current_time = localtime(&s);
 
    sprintf( p, "%02d:%02d:%02d-%s",
           current_time->tm_hour,
           current_time->tm_min,
           current_time->tm_sec,
           status
           );
}



int main() {

    //char WIFI_SSID[] = "wifi01-ei";
    //char WIFI_PASSWORD[] = "Ax32MnF1975-ReB";    //contraseña valida
    stdio_init_all();

     if (cyw43_arch_init()) {
        printf("Wi-Fi init failed");
        return -1;
    }
    cyw43_arch_enable_sta_mode();

    DEBUG_printf("Connecting to WiFi...\n");
    if (cyw43_arch_wifi_connect_timeout_ms(WIFI_SSID, WIFI_PASSWORD, CYW43_AUTH_WPA2_AES_PSK, 30000)) {
        DEBUG_printf("failed to  connect.\n");
        return 1;
    } else {
        DEBUG_printf("*** Connected.*** \n");
    }


    // Create mqtt client
    MQTT_CLIENT_T *my_client = mqtt_client_init();
     
    run_dns_lookup(my_client);
 
    // loop forever
    app_run_in_loop(my_client);
    // ------------

    cyw43_arch_deinit();


    
    return 0;
   
}
